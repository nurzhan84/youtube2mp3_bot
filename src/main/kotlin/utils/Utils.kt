package utils

import org.telegram.telegrambots.api.objects.Update

class Utils{
    companion object {
        fun chatId(update: Update):Long{
            return if(update.message != null){ update.message.chatId }else{ update.callbackQuery.message.chatId }
        }
        fun userId(update: Update):Int{
            return if(update.message != null){ update.message.from.id }else{update.callbackQuery.message.from.id }
        }
        fun messageId(update: Update):Int{
            return if(update.message != null){ update.message.messageId }else{update.callbackQuery.message.messageId }
        }
    }
}