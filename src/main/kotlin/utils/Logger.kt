package utils

class Logger{

    companion object {
        fun i(s: String?) {
            if(s!=null){
                val maxLogSize = 1000
                for (i in 0..s.length / maxLogSize) {
                    val start = i * maxLogSize
                    var end = (i + 1) * maxLogSize
                    end = if (end > s.length) s.length else end
                    println("---------------Mine: " +s.substring(start, end))
                }
            }else{
                println("---------------Mine: " +"null")
            }

        }
    }

}