package dagger.modules

import dagger.Module
import dagger.Provides
import dagger.scopes.AppScope
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor

import java.util.concurrent.TimeUnit
import javax.net.ssl.*
import javax.security.cert.CertificateException

@Module
class NetworkModule{

    @AppScope
    @Provides
    fun interceptor(): Interceptor {
        return Interceptor { chain ->
            val request = chain.request().newBuilder()
            request.addHeader("Content-Type", "application/json")
            request.addHeader("Accept", "application/json")
            chain.proceed(request.build())
        }
    }

    @AppScope
    @Provides
    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        logging.level = HttpLoggingInterceptor.Level.BODY
        //logging.level = HttpLoggingInterceptor.Level.BASIC
        return logging
    }

    @AppScope
    @Provides
    fun sslSocketFactory(trustAllCerts: Array<X509TrustManager>):SSLSocketFactory{
        val sslContext =  SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        return sslContext.socketFactory
    }

    @AppScope
    @Provides
    fun trustManager(): Array<X509TrustManager>{
        return arrayOf<X509TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        })
    }

    @AppScope
    @Provides
    fun hostnameVerifier():HostnameVerifier{
        return HostnameVerifier { s, sslSession -> true }
    }


    @AppScope
    @Provides
    fun okHttpClient(interceptor:Interceptor, httpLoggingInterceptor:HttpLoggingInterceptor,
                     sslSocketFactory:SSLSocketFactory, trustManager: Array<X509TrustManager>,
                     hostnameVerifier: HostnameVerifier):OkHttpClient{
            return OkHttpClient().newBuilder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20,TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .addInterceptor(httpLoggingInterceptor)
                    .sslSocketFactory(sslSocketFactory,trustManager[0])
                    .hostnameVerifier(hostnameVerifier)
                    //.addNetworkInterceptor(interceptor)
                    .build()

    }

}