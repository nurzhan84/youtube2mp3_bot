package dagger.modules

import dagger.Module
import dagger.Provides
import dagger.scopes.AppScope
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class AppModule(val baseUrl: String) {

    @AppScope
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, provideRxAdapter: RxJava2CallAdapterFactory): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(provideRxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
    }

    @AppScope
    @Provides
    fun provideRxAdapter():RxJava2CallAdapterFactory{
        return RxJava2CallAdapterFactory.create()
    }

}