package dagger.modules

import api.GoogleService
import dagger.Module
import dagger.Provides
import dagger.scopes.Runtime
import retrofit2.Retrofit

@Module
class ServiceModule {

    @Runtime
    @Provides
    fun  getApiService(retrofit: Retrofit): GoogleService {
        return retrofit.create(GoogleService::class.java)
    }


}
