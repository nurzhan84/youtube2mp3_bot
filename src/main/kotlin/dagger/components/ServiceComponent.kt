package dagger.components

import dagger.Component
import dagger.modules.ServiceModule
import dagger.scopes.Runtime
import main.Bot

@Runtime
@Component(modules = [ServiceModule::class], dependencies = [AppComponent::class] )
interface ServiceComponent{
    fun injectBot(bot : Bot)
}