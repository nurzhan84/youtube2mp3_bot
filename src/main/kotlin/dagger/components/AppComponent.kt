package dagger.components

import dagger.Component
import dagger.modules.AppModule
import dagger.modules.NetworkModule
import dagger.scopes.AppScope
import retrofit2.Retrofit


@AppScope
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {
    fun getRetrofit(): Retrofit
}