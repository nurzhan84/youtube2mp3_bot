package main

import api.GoogleService
import com.mpatric.mp3agic.Mp3File
import constants.Constants
import dagger.components.DaggerServiceComponent
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.exceptions.TelegramApiException
import utils.Logger
import utils.Utils
import org.telegram.telegrambots.api.methods.send.SendAudio
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import java.io.*

class Bot: TelegramLongPollingBot(){
    init {
        DaggerServiceComponent.builder()
                .appComponent(Main.appComponent)
                .build().injectBot(this)
    }

    @Inject lateinit var apiService: GoogleService
    private val maxFileSize: Long = 52428800 //bytes
    //private val maxFileSize: Long = 30445 //bytes


    override fun getBotToken(): String {
        return Constants.botToken
    }

    override fun getBotUsername(): String {
        return Constants.botUserName
    }

    override fun onUpdateReceived(update: Update?) {
        if(update!=null && update.message!=null){
            val msg = update.message.text
            when(msg){
                "/start" -> onStart(update)
                else -> convertToMP3(update, msg)
            }
        }
    }

    private fun executeSendMessage(sendMessage: SendMessage){
        try {
            execute(sendMessage)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }
    private fun executeUpdateMessage(updateMessage: EditMessageText){
        try {
            execute(updateMessage)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    private fun onStart(update: Update){
        val sendMessage = SendMessage()
        sendMessage.setChatId(Utils.chatId(update))
        sendMessage.text = Constants.give_me_a_link
        executeSendMessage(sendMessage)
    }

    private fun convertToMP3(update: Update, url: String){

        //temporary file name
        val fileId = "${Utils.messageId(update)}_${Utils.userId(update)}"

        val sendMessage = SendMessage()
        sendMessage.setChatId(Utils.chatId(update))
        sendMessage.text = Constants.wait_converting
        executeSendMessage(sendMessage)

        Flowable.just(url)
                .flatMap {it ->
                    Logger.i("url = $it")
                    var id = ""
                    if(it.startsWith("https://www.youtube.com/watch?v=")) {
                        id = it.replace("https://www.youtube.com/watch?v=", "")
                        Logger.i("id_1 = $id")
                        if(id.contains("&")){
                            val parts = id.split("&")
                            id = parts[0]
                            Logger.i("id_1_1 = $id")
                        }
                    }else if(it.startsWith("https://youtu.be/")){
                        id = it.replace("https://youtu.be/","")
                        Logger.i("id_2 = $id")
                    }else if(it.startsWith("https://m.youtube.com/watch?v=")){
                        id = it.replace("https://m.youtube.com/watch?v=", "")
                        Logger.i("id_3 = $id")
                        if(id.contains("&")){
                            val parts = id.split("&")
                            id = parts[0]
                            Logger.i("id_3_1 = $id")
                        }
                    }else if(it.startsWith("http://www.youtube.com/watch?v=")) {
                        id = it.replace("http://www.youtube.com/watch?v=", "")
                        Logger.i("id_4 = $id")
                        if(id.contains("&")){
                            val parts = id.split("&")
                            id = parts[0]
                            Logger.i("id_4_1 = $id")
                        }
                    }else if(it.startsWith("http://youtu.be/")){
                        id = it.replace("http://youtu.be/","")
                        Logger.i("id_5 = $id")
                    }else if(it.startsWith("http://m.youtube.com/watch?v=")){
                        id = it.replace("http://m.youtube.com/watch?v=", "")
                        Logger.i("id_6 = $id")
                        if(id.contains("&")){
                            val parts = id.split("&")
                            id = parts[0]
                            Logger.i("id_6_1 = $id")
                        }
                    }
                    Logger.i("id = $id")
                    Flowable.just(id)
                }
                .concatMap {id ->
                    val link ="https://www.youtube.com/watch?v=$id"
                    val process = Runtime.getRuntime().exec("/usr/local/bin/youtube-dl --no-check-certificate --extract-audio --audio-format mp3 -o /home/ec2-user/temp/$fileId.%(ext)s -f 140 $link")
                    val input = BufferedReader(InputStreamReader(process.inputStream))
                    while(input.readLine() !=null){
                        val line = input.readLine()
                        Logger.i("out = $line")
                    }
                    process.waitFor()

                    Flowable.just(id)
                }
                .concatMap { id ->
                    apiService.getVideoInfo(id,"AIzaSyDhM3Z_ymwNpvhDiOq0myIQsk2Frb0KBh0", "snippet")
                            .flatMap { info ->
                                val fileName = info.items[0].snippet.title.replace(" ","_")
                                        .replace(".","")
                                        .replace("/","")
                                        .replace("\\","")

                                val tempFilePath = "/home/ec2-user/temp/$fileId.mp3"
                                Logger.i("title = $fileName, output =  $tempFilePath")

                                val file = File(tempFilePath)
                                file.renameTo(File(tempFilePath.replace("$fileId.mp3","$fileName.mp3")))

                                Flowable.just(fileName)
                            }
                }
                .concatMap { fileName ->
                    val outputFiles = ArrayList<String>()
                    val outputPath = "/home/ec2-user/temp/$fileName.mp3"
                    val file = File(outputPath)
                    val fileSize = getFileSize(file)

                    Logger.i("fileSize = $fileName = $fileSize , maxSize = $maxFileSize")

                    if(fileSize > maxFileSize){
                        val div = (fileSize*1f/maxFileSize).toInt() + 1
                        val duration = getDuration(outputPath)
                        val minDuration = duration/div

                        Logger.i("div = $div, duration = $duration, minDuration = $minDuration")

                        val command = "/usr/local/bin/ffmpeg/ffmpeg -i $outputPath -f segment -segment_time $minDuration -c copy /home/ec2-user/temp/part%d-$fileName.mp3"
                        val process = Runtime.getRuntime().exec(command)
                        val input = BufferedReader(InputStreamReader(process.inputStream))
                        while(input.readLine() !=null){
                            val line = input.readLine()
                            Logger.i("out = $line")
                        }
                        process.waitFor()

                        /*loop@ for(index in 0 .. div){
                            val partsPath = "/home/ec2-user/temp/part$index-$fileName.mp3"
                            if(File(partsPath).exists()){
                                outputFiles.add(partsPath)
                            }else{break@loop}
                        }*/

                        var index = 0
                        var fileExists = true
                        while (fileExists){
                            val partsPath = "/home/ec2-user/temp/part$index-$fileName.mp3"
                            if(File(partsPath).exists()){
                                outputFiles.add(partsPath)
                            }else{
                                fileExists = false
                            }
                            index++
                        }
                    }else{
                        outputFiles.add(outputPath)
                    }
                    Flowable.fromArray(renameFiles(outputFiles, fileName))
                }
                .concatMap {outputFiles ->
                    val sendAudioList = ArrayList<SendAudio>()
                    for(outputPath in outputFiles){
                        val audioMessage = SendAudio()
                        audioMessage.setChatId(Utils.chatId(update))
                        audioMessage.setNewAudio(File(outputPath))
                        audioMessage.performer = outputPath.replace("/home/ec2-user/temp/","").replace(".mp3","")
                        audioMessage.title = outputPath.replace("/home/ec2-user/temp/","").replace(".mp3","")
                        sendAudioList.add(audioMessage)
                    }

                    Flowable.fromArray(sendAudioList)
                }
                .retryWhen { h -> h.flatMap { e ->
                    Logger.i(" error $e")
                    if(e is HttpException || e is UnknownHostException || e is SocketTimeoutException) {
                        Flowable.timer(10, TimeUnit.SECONDS)
                    }else{
                        Flowable.error(e)
                    }
                }}
                .subscribeOn(Schedulers.io())
                .subscribe({result ->
                    Logger.i("result = ${result.size}")
                    for (message in result){
                        sendAudio(message)
                    }
                },{error ->
                    Logger.i("error = ${error.message}")
                    val errorMessage = SendMessage()
                    errorMessage.setChatId(Utils.chatId(update))
                    errorMessage.text = Constants.error_while_converting
                    executeSendMessage(errorMessage)
                })
    }

    private fun renameFiles(outputFiles: ArrayList<String>, fileName:String):ArrayList<String>{
        val outputFilesRenamed = ArrayList<String>()
        val fileNameRenamed = fileName.replace("_"," ")
        for(path in outputFiles){
            val file = File(path)
            val renamedPath = path.replace("$fileName.mp3","$fileNameRenamed.mp3")
            file.renameTo(File(renamedPath))
            outputFilesRenamed.add(renamedPath)
        }
        return outputFilesRenamed
    }

    private fun getDuration(filePath: String):Long{
        val mp3file = Mp3File(filePath)
        return Math.abs(mp3file.lengthInSeconds)
    }

    private fun getFileSize(file: File):Long{
        return file.length()
    }

}

