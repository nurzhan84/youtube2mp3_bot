package main
import api.GoogleService
import dagger.components.AppComponent
import dagger.components.DaggerAppComponent
import dagger.modules.AppModule
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi
import org.telegram.telegrambots.exceptions.TelegramApiException



fun main(args: Array<String>) {

    createAppComponent()
    ApiContextInitializer.init()
    val botApi = TelegramBotsApi()
    try {
        botApi.registerBot(Bot())
    } catch (e: TelegramApiException) {
        e.printStackTrace()
    }


  /*  var path = ".\\temp\\"
    val web = URL("https://www.youtube.com/watch?v=uSodCuo73ig")

    //val user = YouTubeQParser(YouTubeInfo.YoutubeQuality.p480)
    val user = VGet.parser(web)
    val videoInfo = user.info(web)
    val notify = VGetStatus(videoInfo)
    val file = File(path)
    val v = VGet(videoInfo, file)
    //v.extract(user, AtomicBoolean(false), notify)
    v.download(user, AtomicBoolean(false), notify)*/

    /*val process = Runtime.getRuntime().exec("E:\\work\\youtube_mp3\\youtube-dl.exe -o \"E:\\work\\youtube_mp3\\temp/%(title)s.%(ext)s\" -f 140 https://www.youtube.com/watch?v=uSodCuo73ig")
    Thread(Runnable {
        val input = BufferedReader(InputStreamReader(process.inputStream))
        var line: String? = ""

        try {

            while (line != null){
                line = input.readLine()
                println(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }).start()
    process.waitFor()*/
}

private fun createAppComponent(){
    Main.appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(GoogleService.baseUrl))
            .build()
}

class Main{
    companion object {
        lateinit var appComponent: AppComponent
    }
}