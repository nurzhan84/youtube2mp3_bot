package api

import io.reactivex.Flowable
import models.VideoInfo
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*


interface GoogleService{

    companion object {
        const val baseUrl = "https://www.googleapis.com"
        const val telegramBaseUrl = "https://api.telegram.org"
    }

    @GET("/youtube/v3/videos")
    fun getVideoInfo(@Query("id")id: String, @Query("key")key: String, @Query("part")part: String): Flowable<VideoInfo>

    @GET
    @Streaming
    fun getAudioFile(@Url url:String): Flowable<ResponseBody>

    @Multipart
    @POST
    fun sendAudio(@Url url:String, @Query("chat_id")chat_id: String, @Part file: MultipartBody.Part): Flowable<Any>
}