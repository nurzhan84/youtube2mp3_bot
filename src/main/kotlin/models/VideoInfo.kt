package models

//https://www.googleapis.com/youtube/v3/videos?id=uSodCuo73ig&key=AIzaSyDhM3Z_ymwNpvhDiOq0myIQsk2Frb0KBh0&part=snippet

data class VideoInfo(
		val kind: String, //youtube#videoListResponse
		val etag: String, //"RmznBCICv9YtgWaaa_nWDIH1_GM/S6BxALN8qDq5l_h0vWrBlKd5maI"
		val pageInfo: PageInfo,
		val items: List<Item>
)

data class Item(
		val kind: String, //youtube#video
		val etag: String, //"RmznBCICv9YtgWaaa_nWDIH1_GM/Y0OWJc7USPAJTMNBN6cMQ1mFOOY"
		val id: String, //uSodCuo73ig
		val snippet: Snippet
)

data class Snippet(
		val publishedAt: String, //2013-04-09T18:02:38.000Z
		val channelId: String, //UCv_UznKRM9rkqi1r2jMzLBA
		val title: String, //🔴  Brotherhood of Man  ✙  ♏otorhead ✙  (subtitles lirics)
		val description: String,
		val thumbnails: Thumbnails,
		val channelTitle: String, //Maks CREST
		val tags: List<String>,
		val categoryId: String, //22
		val liveBroadcastContent: String, //none
		val localized: Localized,
		val defaultAudioLanguage: String //en-US
)

data class Localized(
		val title: String, //🔴  Brotherhood of Man  ✙  ♏otorhead ✙  (subtitles lirics)
		val description: String
)

data class Thumbnails(
		val default: Default,
		val medium: Medium,
		val high: High,
		val standard: Standard,
		val maxres: Maxres
)

data class Medium(
		val url: String, //https://i.ytimg.com/vi/uSodCuo73ig/mqdefault.jpg
		val width: Int, //320
		val height: Int //180
)

data class Standard(
		val url: String, //https://i.ytimg.com/vi/uSodCuo73ig/sddefault.jpg
		val width: Int, //640
		val height: Int //480
)

data class Default(
		val url: String, //https://i.ytimg.com/vi/uSodCuo73ig/default.jpg
		val width: Int, //120
		val height: Int //90
)

data class High(
		val url: String, //https://i.ytimg.com/vi/uSodCuo73ig/hqdefault.jpg
		val width: Int, //480
		val height: Int //360
)

data class Maxres(
		val url: String, //https://i.ytimg.com/vi/uSodCuo73ig/maxresdefault.jpg
		val width: Int, //1280
		val height: Int //720
)

data class PageInfo(
		val totalResults: Int, //1
		val resultsPerPage: Int //1
)